from django.urls import path

from container_apps.web.views import IndexView

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
]
